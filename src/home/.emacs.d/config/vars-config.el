(setq user-config-dir (getenv "XDG_CONFIG_HOME"))
(setq user-data-dir (getenv "XDG_DATA_HOME"))
(setq user-cache-dir (getenv "XDG_CACHE_HOME"))

;(setq browse-url-generic-program
;      (executable-find (getenv "BROWSER"))
;      browse-url-browser-function 'browse-url-generic)

(provide 'vars-config)


