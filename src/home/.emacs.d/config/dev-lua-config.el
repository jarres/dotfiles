(use-package lua-mode
  :ensure t
  :mode ("\\.lua\\'" . lua-mode))

(provide 'dev-lua-config)
