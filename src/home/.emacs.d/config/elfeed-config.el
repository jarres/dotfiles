(use-package elfeed
  :ensure t
  :config
  (setq elfeed-db-directory (expand-file-name "elfeed" user-cache-dir))
  (setq elfeed-feeds '(("pragmaticemacs.com/feed/"      emacs blog)
		       ("https://nullprogram.com/feed/" emacs blog)
		       ("https://lwn.net/headlines/rss" linux news)
		       ("https://www.linux.org.ru/section-rss.jsp?section=1" linux lor news)
		       ("https://www.linux.org.ru/section-rss.jsp?section=2" linux lor forum)))
  :bind ("C-x r" . elfeed))

(defadvice elfeed-search-update (before lwn activate)
  (let ((feed (elfeed-db-get-feed "https://lwn.net/headlines/rss")))
    (setf (elfeed-feed-title feed) "lwn.net")))

(defadvice elfeed-search-update (before lorn activate)
  (let ((feed (elfeed-db-get-feed "https://www.linux.org.ru/section-rss.jsp?section=1")))
    (setf (elfeed-feed-title feed) "linux.org.ru [n]")))

(defadvice elfeed-search-update (before lorf activate)
  (let ((feed (elfeed-db-get-feed "https://www.linux.org.ru/section-rss.jsp?section=2")))
    (setf (elfeed-feed-title feed) "linux.org.ru [f]")))

(provide 'elfeed-config)
