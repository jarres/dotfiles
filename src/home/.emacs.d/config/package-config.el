(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/")    t)
(add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/") t)
(setq package-enable-at-startup nil)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(provide 'package-config)
