(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(global-display-line-numbers-mode)
(global-hl-line-mode 1)

(setq echo-keystrokes 0.1)
(setq auto-save-default nil)
(setq make-backup-files nil)

(provide 'common-config)

