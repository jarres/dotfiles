(require 'notmuch)

(setq notmuch-show-logo nil
      notmuch-hello-auto-refresh t
      notmuch-show-all-tags-list nil
      notmuch-show-empty-saved-searches t
      notmuch-search-oldest-first nil)

(setq notmuch-hello-sections '(notmuch-hello-insert-header
			       notmuch-hello-insert-saved-searches
			       notmuch-hello-insert-search
			       notmuch-hello-insert-recent-searches))

(setq notmuch-saved-searches '((:name "gmail/inbox" :query "tag:gmail and tag:inbox" :count-query "tag:gmail and tag:inbox and tag:unread")
			       (:name "gmail/sent"  :query "tag:gmail and tag:sent"  :count-query "tag:gmail and tag:sent")
			       (:name "gmail/spam"  :query "tag:gmail and tag:spam"  :count-query "tag:gmail and tag:spam and tag:unread")
			       (:name "gmail/draft" :query "tag:gmail and tag:draft" :count-query "tag:gmail and tag:draft")
			       (:name "gmail/junk"  :query "tag:gmail and tag:junk"  :count-query "tag:gmail and tag:junk and tag:unread")

			       (:name "work/inbox"  :query "tag:work and tag:inbox"  :count-query "tag:work and tag:inbox and tag:unread")
			       (:name "work/sent"   :query "tag:work and tag:sent"   :count-query "tag:work and tag:sent")
			       (:name "work/spam"   :query "tag:work and tag:spam"   :count-query "tag:work and tag:spam and tag:unread")
			       (:name "work/draft"  :query "tag:work and tag:draft"  :count-query "tag:work and tag:draft")
			       (:name "work/junk"   :query "tag:work and tag:junk"   :count-query "tag:work and tag:junk and tag:unread")))

(provide 'notmuch-config)
