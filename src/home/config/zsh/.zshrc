#-----------------------------------------------------------------------
#                         navigation
#-----------------------------------------------------------------------

setopt AUTO_CD				# Change directories without 'cd'
setopt CHASE_LINKS			# Resolve symbolic links to their true values when changing directory.
setopt EXTENDED_GLOB			# Treat the ‘#’, ‘~’ and ‘^’ characters as part of patterns. 
setopt GLOB_DOTS			# Do not require a leading ‘.’ in a filename to be matched explicitly. 

#-----------------------------------------------------------------------
#                         history
#-----------------------------------------------------------------------

HISTFILE="${XDG_DATA_HOME}/zsh/history"	# The file to save the history in when an interactive shell exits.
HISTSIZE=8192				# The maximum number of events stored in the internal history list.
SAVEHIST=8192				# The maximum number of history events to save in the history file. 

setopt EXTENDED_HISTORY			# Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY		# Write to the history file immediately.
setopt SHARE_HISTORY			# Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST		# Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS			# Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS		# Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS		# Do not display a line previously found.
setopt HIST_IGNORE_SPACE		# Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS		# Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS		# Remove superfluous blanks before recording entry.

#-----------------------------------------------------------------------
#                         completion
#-----------------------------------------------------------------------

autoload -U compinit
compinit -d "${XDG_DATA_HOME}/zsh/compdump"

zstyle ':completion:*:*:*:*:*'	    menu select
zstyle ':completion:*:*:default'    force-list always
zstyle ':completion:*'              rehash true
zstyle ':completion::complete:*'    use-cache 1
zstyle ':completion::complete:*'    gain-privileges 1

zstyle ':completion:*:processes'    sort false
zstyle ':completion:*:processes'    command 'ps -au $USER'
zstyle ':completion:*:processes'    insert-ids menu select

#-----------------------------------------------------------------------
#                         prompt
#-----------------------------------------------------------------------

autoload -U promptinit
promptinit

prompt gentoo

#-----------------------------------------------------------------------
#                         bindings
#-----------------------------------------------------------------------

case $TERM in
    linux)
	bindkey  "^[[1~"	beginning-of-line
	bindkey  "^[[4~"	end-of-line
	bindkey  "^[[2~"	overwrite-mode
	bindkey  "^[[3~"	delete-char
	bindkey  "^[[A"		up-line-or-history
	bindkey  "^[[B"		down-line-or-history
	bindkey  "^[[D"		backward-char
	bindkey  "^[[C"		forward-char
	bindkey  "^[[5~"	beginning-of-buffer-or-history
	bindkey  "^[[6~"	end-of-buffer-or-history 
    ;;
    xterm-termite)
	bindkey  "^[[H"		beginning-of-line
	bindkey  "^[[F"		end-of-line
	bindkey  "^[[2~"	overwrite-mode
	bindkey  "^[[3~"	delete-char
	bindkey  "^[[A"		up-line-or-history
	bindkey  "^[[B"		down-line-or-history
	bindkey  "^[[D"		backward-char
	bindkey  "^[[C"		forward-char
	bindkey  "^[[5~"	beginning-of-buffer-or-history
	bindkey  "^[[6~"	end-of-buffer-or-history 
    ;;
esac

#-----------------------------------------------------------------------
#                         aliases
#-----------------------------------------------------------------------

alias cp="nocorrect cp --verbose --preserve=all"
alias mv="nocorrect mv --verbose --interactive"
alias rm="nocorrect rm --verbose --interactive"

alias ls="ls --almost-all --color --human-readable --group-directories-first"
alias df="df --human-readable --exclude-type=tmpfs"
alias du="du --human-readable --total --max-depth=1 | sort --human-numeric-sort"

alias grep='grep --color --line-number --with-filename'

alias nv="nvim"
alias em="emacs"

