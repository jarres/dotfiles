#-----------------------------------------------------------------------
#			  functions
#-----------------------------------------------------------------------

function ensure_dir() {
    [ -d "$1" ] || mkdir --mode="0700" --parents "$1"
}

function ensure_file() {
    [ -f "$1" ] || install --mode "0600" /dev/null "$1"
}

#-----------------------------------------------------------------------
#			  defaults
#-----------------------------------------------------------------------

export EDITOR=emacs
export PAGER=less
export BROWSER=firefox

#-----------------------------------------------------------------------
#			  xdg compatibility
#-----------------------------------------------------------------------

# gnupg
export GNUPGHOME=${XDG_CONFIG_HOME}/gnupg

# pass
export PASSWORD_STORE_DIR=${XDG_DATA_HOME}/passwords

# notmuch
export NOTMUCH_CONFIG=${XDG_CONFIG_HOME}/notmuch/config

# wget
export WGETRC=${XDG_CONFIG_HOME}/wget/config

# less
export LESSKEY=${XDG_CONFIG_HOME}/less/lesskey
export LESSHISTFILE=${XDG_DATA_HOME}/less/history

# java
export _JAVA_OPTIONS="${_JAVA_OPTIONS} -Duser.share=${XDG_DATA_HOME}"
export _JAVA_OPTIONS="${_JAVA_OPTIONS} -Djava.util.prefs.userRoot=${XDG_DATA_HOME}/java"

# jetbrains
export RIDER_PROPERTIES=${XDG_CONFIG_HOME}/jetbrains/rider.properties
export DATAGRIP_PROPERTIES=${XDG_CONFIG_HOME}/jetbrains/datagrip.properties

#-----------------------------------------------------------------------
#			  directories
#-----------------------------------------------------------------------

ensure_dir "$XDG_DATA_HOME"
ensure_dir "$XDG_CACHE_HOME"

ensure_dir "${XDG_DATA_HOME}/passwords"
ensure_dir "${XDG_DATA_HOME}/mailbox"

ensure_dir "${XDG_DATA_HOME}/less"
ensure_dir "${XDG_DATA_HOME}/zsh"

ensure_dir "${XDG_DATA_HOME}/passwords"
ensure_dir "${XDG_DATA_HOME}/screenshots"
